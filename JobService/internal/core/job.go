package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type Job struct {
	ID           primitive.ObjectID `bson:"_id,omitempty"`
	Location     string             `bson:"location,omitempty"`
	ActivityType string             `bson:"activityType,omitempty"`
	Description  string             `bson:"description,omitempty"`
}
