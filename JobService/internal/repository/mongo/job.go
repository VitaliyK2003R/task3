package mongo

import (
	"JobService/internal/core"
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type JobRepository struct {
	collection *mongo.Collection
}

func NewJobRepository(collection *mongo.Collection) *JobRepository {
	return &JobRepository{collection: collection}
}

func (repository *JobRepository) GetByActivity(ctx context.Context, activityType string) (*core.Job, error) {
	ctxTimeout, cancel := context.WithTimeout(ctx, time.Second*5)
	defer cancel()

	jobChannel := make(chan *core.Job)
	var err error

	go func() {
		err = repository.retrieveJob(ctxTimeout, activityType, jobChannel)
	}()

	if err != nil {
		return nil, err
	}

	var job *core.Job

	select {
	case <-ctxTimeout.Done():
		fmt.Println("Processing timeout in Mongo")
		break
	case job = <-jobChannel:
		fmt.Println("Finished processing in Mongo")
	}

	return job, nil
}

func (repository *JobRepository) retrieveJob(ctx context.Context, activityType string, channel chan<- *core.Job) (err error) {

	job := &core.Job{}

	//TODO: ошибка после FindOne не ловится
	err = repository.collection.FindOne(ctx, bson.M{"activityType": activityType}).Decode(job)

	if err != nil {
		return err
	}

	channel <- job

	return nil
}
