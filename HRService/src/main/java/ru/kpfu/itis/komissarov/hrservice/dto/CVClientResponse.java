package ru.kpfu.itis.komissarov.hrservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CVClientResponse {
    private String name;
    private String activityType;
    private String summary;
}
