package ru.kpfu.itis.komissarov.hrservice.service;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.komissarov.hrservice.dto.CVClientResponse;
import ru.kpfu.itis.komissarov.hrservice.dto.JobClientResponse;
import ru.kpfu.itis.komissarov.hrservice.dto.MatchResponse;
import ru.kpfu.itis.komissarov.hrservice.grpc.pb.job.JobResponse;
import ru.kpfu.itis.komissarov.hrservice.util.mapper.CVMapper;
import ru.kpfu.itis.komissarov.hrservice.util.mapper.JobMapper;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HRServiceImpl implements HRService {
    private final JobService jobService;
    private final CVService cvService;

    @Override
    public ResponseEntity<List<MatchResponse>> getAllMatchesByActivity() {
        List<MatchResponse> matchResponses = new ArrayList<>();
        List<CVClientResponse> cvClientResponses = cvService.getAll();

        for (CVClientResponse cvClientResponse: cvClientResponses) {

            String activityType = cvClientResponse.getActivityType();
            JobClientResponse jobClientResponse = jobService.getJobByActivityType(activityType);
            MatchResponse matchResponse = new MatchResponse(jobClientResponse, cvClientResponse);
            matchResponses.add(matchResponse);
        }

        return new ResponseEntity<>(matchResponses, HttpStatus.OK);
    }
}
