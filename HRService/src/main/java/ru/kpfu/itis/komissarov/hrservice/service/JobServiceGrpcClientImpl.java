package ru.kpfu.itis.komissarov.hrservice.service;

import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.komissarov.hrservice.dto.JobClientResponse;
import ru.kpfu.itis.komissarov.hrservice.grpc.pb.job.JobRequest;
import ru.kpfu.itis.komissarov.hrservice.grpc.pb.job.JobServiceGrpc;
import ru.kpfu.itis.komissarov.hrservice.util.mapper.JobMapper;

@Service
@RequiredArgsConstructor
public class JobServiceGrpcClientImpl implements JobService {
    private final JobMapper jobMapper;
    @GrpcClient("job-service")
    private JobServiceGrpc.JobServiceBlockingStub jobService;

    @Override
    public JobClientResponse getJobByActivityType(String activityType) {
        return jobMapper.toJobClientResponseFromJobResponse(jobService.getJob(JobRequest.newBuilder().setActivityType(activityType).build()));
    }
}
