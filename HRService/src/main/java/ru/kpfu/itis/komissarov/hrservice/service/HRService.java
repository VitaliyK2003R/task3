package ru.kpfu.itis.komissarov.hrservice.service;

import org.springframework.http.ResponseEntity;
import ru.kpfu.itis.komissarov.hrservice.dto.MatchResponse;

import java.util.List;

public interface HRService {
    ResponseEntity<List<MatchResponse>> getAllMatchesByActivity();
}
