package ru.kpfu.itis.komissarov.hrservice.util.mapper;

import org.springframework.stereotype.Component;
import ru.kpfu.itis.komissarov.hrservice.dto.CVClientResponse;
import ru.kpfu.itis.komissarov.hrservice.grpc.pb.cv.CVResponse;

import java.util.ArrayList;
import java.util.List;

@Component
public class CVMapper {
    public CVClientResponse toCVClientResponseFromCVResponse(CVResponse cvResponse) {
        return new CVClientResponse(cvResponse.getName(), cvResponse.getActivityType(), cvResponse.getSummary());
    }

    public List<CVClientResponse> toListCVClientResponsesFromList(List<CVResponse> cvResponses) {
        List<CVClientResponse> cvClientResponses = new ArrayList<>();
        for (CVResponse cvResponse: cvResponses) {
            cvClientResponses.add(toCVClientResponseFromCVResponse(cvResponse));
        }
        return cvClientResponses;
    }
}
