package ru.kpfu.itis.komissarov.hrservice.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.komissarov.hrservice.dto.MatchResponse;
import ru.kpfu.itis.komissarov.hrservice.service.HRService;

import java.util.List;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class HRController {
    private final HRService hrService;

    @GetMapping("/candidates")
    public ResponseEntity<List<MatchResponse>> getMatchesByActivity() {
        return hrService.getAllMatchesByActivity();
    }
}
