package ru.kpfu.itis.komissarov.hrservice.util.mapper;

import org.springframework.stereotype.Component;
import ru.kpfu.itis.komissarov.hrservice.dto.JobClientResponse;
import ru.kpfu.itis.komissarov.hrservice.grpc.pb.job.JobResponse;

@Component
public class JobMapper {
    public JobClientResponse toJobClientResponseFromJobResponse(JobResponse jobResponse) {
        if (jobResponse == null) return new JobClientResponse();
        return new JobClientResponse(jobResponse.getLocation(),
                                        jobResponse.getActivityType(),
                                        jobResponse.getDescription());
    }
}
