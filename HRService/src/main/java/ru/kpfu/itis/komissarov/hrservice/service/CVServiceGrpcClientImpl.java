package ru.kpfu.itis.komissarov.hrservice.service;

import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.komissarov.hrservice.dto.CVClientResponse;
import ru.kpfu.itis.komissarov.hrservice.grpc.pb.cv.CVRequest;
import ru.kpfu.itis.komissarov.hrservice.grpc.pb.cv.CVResponse;
import ru.kpfu.itis.komissarov.hrservice.grpc.pb.cv.CVServiceGrpc;
import ru.kpfu.itis.komissarov.hrservice.grpc.pb.cv.EmptyRequest;
import ru.kpfu.itis.komissarov.hrservice.util.mapper.CVMapper;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CVServiceGrpcClientImpl implements CVService {

    @GrpcClient("cv-service")
    private CVServiceGrpc.CVServiceBlockingStub cvService;
    private final CVMapper cvMapper;

    @Override
    public CVClientResponse getCVByActivityType(String activityType) {
        return cvMapper.toCVClientResponseFromCVResponse(cvService.getCV(CVRequest.newBuilder().setActivityType(activityType).build()));
    }

    @Override
    public List<CVClientResponse> getAll() {
        return cvMapper.toListCVClientResponsesFromList(cvService.getAll(EmptyRequest.newBuilder().build()).getCvsList());
    }
}
